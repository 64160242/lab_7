package com.phil.week8;

public class Tree {
    //Attributes
    private String name;
    private int x;
    private int y;

    public Tree(String name, int x, int y){ // Construtor
        this.name = name;
        this.x = x;
        this.y = y;
    }// Methodes
    public void print(){
        System.out.println("Tree: "+ name + " X:"+ x +" Y:"+ y);
    }
}
